package data

import "github.com/GeertJohan/go.rice"

func GetDefaultImage(name string) ([]byte, error) {
	viewBox, err := rice.FindBox("default-images")
	if err != nil {
		return nil, err
	}
	return viewBox.Bytes("default.png")
}
