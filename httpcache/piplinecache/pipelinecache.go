package piplinecache

import (
	"gitlab.com/golibs/imageproxy/httpcache"
)

type Cache struct {
	nextCache httpcache.Cache
}

func New(nextCache httpcache.Cache) httpcache.Cache {

	return &Cache{
		nextCache: nextCache,
	}
}
func (c Cache) Get(key string) ([]byte, bool) {
	return c.nextCache.Get(key)
}
func (c Cache) Set(key string, bytes []byte) {
	c.nextCache.Set(key, bytes)
}
func (c Cache) Delete(key string) {
	c.nextCache.Delete(key)
}
