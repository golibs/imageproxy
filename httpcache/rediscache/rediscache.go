// Package redis provides a redis interface for http caching.
package rediscache

import (
	"github.com/go-redis/redis"
	"time"
	"gitlab.com/golibs/imageproxy/httpcache"
)

// cache is an implementation of httpcache.Cache that caches responses in a
// redis server.
type cache struct {
	client *redis.Client
}

// cacheKey modifies an httpcache key for use in redis. Specifically, it
// prefixes keys to avoid collision with other data stored in redis.
func cacheKey(key string) string {
	return "image-proxy:" + key
}

// Get returns the response corresponding to key if present.
func (c cache) Get(key string) (resp []byte, ok bool) {
	item, err := c.client.Get(cacheKey(key)).Bytes()
	if err != nil {
		return nil, false
	}
	return item, true
}

// Set saves a response to the cache as key.
func (c cache) Set(key string, resp []byte) {
	c.client.Set(key, resp, time.Minute*30)
}

// Delete removes the response with key from the cache.
func (c cache) Delete(key string) {
	c.client.Del(cacheKey(key))
}

// NewWithClient returns a new Cache with the given redis connection.
func NewWithClient(client *redis.Client) httpcache.Cache {
	return cache{client}
}
