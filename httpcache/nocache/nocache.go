package nocache

var NopCache = new(nopCache)

type nopCache struct{}

func (c nopCache) Get(string) ([]byte, bool) { return nil, false }
func (c nopCache) Set(string, []byte)        {}
func (c nopCache) Delete(string)             {}
