package filecache

import (
	"log"
	"gitlab.com/golibs/helper/text"
	"path"
	"os"
	"io/ioutil"
	"net/url"
	//"bufio"
	//"bytes"
	//"net/http"
	"net/http/httputil"
	"net/http"
	"bytes"
	"bufio"
	"gitlab.com/golibs/imageproxy"
)

type Cache struct {
	BasePath string
}

func (c *Cache) Get(key string) (resp []byte, ok bool) {
	absolutePath := c.keyToFilename(key)
	response, err := imageproxy.GetResponseFromFile(absolutePath, true)
	ok = err == nil
	defer log.Println("[Filecache] Get [", absolutePath, "]", "ok=", ok)
	if ok {
		defer response.Body.Close()
		resp, err = httputil.DumpResponse(response, true)
		return resp, err == nil
	}
	return nil, ok
}
func (c *Cache) getAbsolutePath(basePath string) string {
	return path.Join(c.BasePath, basePath)
}

// Set saves a response to the cache as key
func (c *Cache) Set(key string, resp []byte) {
	var err error
	absolutePath := c.keyToFilename(key)
	log.Println("[Filecache] Set [", absolutePath, "]")
	baseDir := path.Dir(absolutePath)
	os.MkdirAll(baseDir, os.ModePerm)
	b := bytes.NewBuffer(resp)
	response, err := http.ReadResponse(bufio.NewReader(b), nil)
	if err == nil {
		defer response.Body.Close()
		bytes, err := ioutil.ReadAll(response.Body)
		if err == nil {
			err = ioutil.WriteFile(absolutePath, bytes, os.ModePerm)
			return
		}

	}

	err = ioutil.WriteFile(absolutePath, resp, os.ModePerm)
	if err != nil {
		log.Println("Can not save file :", absolutePath, ",error:", err)
	}
}

// Delete removes the response with key from the cache
func (c *Cache) Delete(key string) {
	absolutePath := c.keyToFilename(key)
	log.Println("[Filecache] Delete [", absolutePath, "]")
	err := os.Remove(absolutePath)
	if err != nil {
		log.Println("Can not remove file :", absolutePath)
	}
}

func (c *Cache) keyToFilename(key string) string {
	parse, err := url.Parse(key)
	if err != nil {
		return c.getAbsolutePath(text.Slug(key))
	}
	dir, fileName := path.Split(parse.Path)
	if parse.Fragment != "" {
		fileName = parse.Fragment + "-" + fileName
	}
	return c.getAbsolutePath(path.Join(parse.Host, dir, fileName))
}

func New(basePath string) *Cache {
	return &Cache{
		BasePath: basePath}
}
