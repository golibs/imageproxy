package imageproxy

import (
	"net/http"
	"bufio"
	"bytes"
	//"time"
	//"os"
	"log"
	"regexp"
	"io/ioutil"
	"net"
	"time"
	"net/http/httputil"
	"gitlab.com/golibs/imageproxy/data"
	"crypto/tls"
)

func DefaultImageResponseTransport() (http.RoundTripper, error) {
	contents, err := data.GetDefaultImage("default.png")
	if err != nil {
		return nil, err
	}
	return ImageResponseTransport(contents)
}
func ImageResponseTransport(defaultImageContents []byte) (http.RoundTripper, error) {

	defaultResponse := ConvertBytesToHttpResponse(defaultImageContents)
	defaultResponse.Header = http.Header{
		"Cache-Control": []string{"no-store"},
	}
	contents, err := httputil.DumpResponse(defaultResponse, true)
	transport := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		MaxIdleConns:          400,
		IdleConnTimeout:       30 * time.Second,
		TLSHandshakeTimeout:   4 * time.Second,
		ExpectContinueTimeout: 2 * time.Second,
	}
	return &defaultResponseTransport{
		contents:        contents,
		AcceptFileTypes: regexp.MustCompile("image/(pjpeg|jpeg|(x-)?png)"),
		transport:       transport,
	}, err

}

type defaultResponseTransport struct {
	// Transport is the underlying http.RoundTripper used to satisfy
	// non-transform requests (those that do not include a URL fragment).
	remoteFetch     bool
	baseUrl         string
	transport       http.RoundTripper
	contents        []byte
	AcceptFileTypes *regexp.Regexp
}

// RoundTrip implements the http.RoundTripper interface.
func (t *defaultResponseTransport) RoundTrip(req *http.Request) (*http.Response, error) {

	response, err := t.transport.RoundTrip(req)
	if err == nil {
		validStatus := response.StatusCode == 304 || response.StatusCode >= 200 && response.StatusCode < 300
		contentType := response.Header.Get("Content-Type")
		if contentType != "" {
			if validStatus && t.AcceptFileTypes.MatchString(contentType) {
				return response, err
			}
		} else {
			contents, err := ioutil.ReadAll(response.Body)
			if err == nil {
				detectContentType := http.DetectContentType(contents)
				if t.AcceptFileTypes.MatchString(detectContentType) {
					response.Body = ioutil.NopCloser(bufio.NewReader(bytes.NewBuffer(contents)))
					return response, err
				}
			}

		}
	}
	if response != nil {
		response.Body.Close()
	}
	log.Printf(" Fetch default Response : url=%s, error:%s", req.URL.String(), err)
	res, err := http.ReadResponse(bufio.NewReader(bytes.NewBuffer(t.contents)), req)
	res.Header.Add("Date", time.Now().UTC().Format(time.RFC1123))
	res.Header.Add("default-response", "true")
	return res, err
}
