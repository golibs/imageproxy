package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"gitlab.com/golibs/imageproxy/httpcache/filecache"
	"gitlab.com/golibs/imageproxy"
	"gitlab.com/golibs/imageproxy/httpcache"
	"time"
	"gitlab.com/golibs/imageproxy/httpcache/lrucache"
)

var addr = flag.String("addr", "localhost:8092", "TCP address to listen on")
var whitelist = flag.String("whitelist", "", "comma separated list of allowed remote hosts")
var referrers = flag.String("referrers", "", "comma separated list of allowed referring hosts")
var baseURL = flag.String("baseURL", "", "default base URL for relative remote URLs")
var signatureKey = flag.String("signatureKey", "", "HMAC key used in calculating request signatures")
var scaleUp = flag.Bool("scaleUp", false, "allow images to scale beyond their original dimensions")
var timeout = flag.Duration("timeout", 0, "time limit for requests served by this proxy")

func main() {
	flag.Parse()
	downloadCache := filecache.New(imageproxy.GetDir("./cache-dir/download"))
	//resizeCache := filecache.New(imageproxy.GetDir("./cache-dir/resize"))
	resizeCache := lrucache.New(500)

	defaultTransport, err := imageproxy.DefaultImageResponseTransport()
	if err != nil {
		panic(err)
	}
	downloadClient := new(http.Client)
	downloadClient.Transport = &httpcache.Transport{
		Transport:           defaultTransport,
		Cache:               downloadCache,
		MarkCachedResponses: true,
	}
	resizeClient := new(http.Client)
	resizeClient.Transport = &httpcache.Transport{
		Transport: &imageproxy.TransformingTransport{
			Transport:     http.DefaultTransport,
			CachingClient: downloadClient,
			Log: func(format string, v ...interface{}) {
				log.Printf(format, v...)

			},
		},
		CanStore: func(reqCacheControl, respCacheControl map[string]string) (bool) {
			return true
		},
		OnResponse: func(req *http.Request, res *http.Response) {
			if res.Header.Get("default-response") == "true" {
				req.Header.Set("Cache-Control", "only-if-cached")
				res.Header.Set("Cache-Control", "only-if-cached")
			}
		},
		Cache:               resizeCache,
		MarkCachedResponses: true,
	}

	p := &imageproxy.Proxy{
		Client: resizeClient,
		TransformNameFunc: func(requestUrl string) string {
			return requestUrl
			//return "https://img.websosanh.vn/v2/users/wss/images/jWmg4zCodgFX.jpg?width=220&compress=85"
		},
		Timeout: time.Second * 30,
	}
	//if *whitelist != "" {
	//	p.Whitelist = strings.Split(*whitelist, ",")
	//}
	//if *referrers != "" {
	//	p.Referrers = strings.Split(*referrers, ",")
	//}
	//if *signatureKey != "" {
	//	key := []byte(*signatureKey)
	//	if strings.HasPrefix(*signatureKey, "@") {
	//		file := strings.TrimPrefix(*signatureKey, "@")
	//		var err error
	//		key, err = ioutil.ReadFile(file)
	//		if err != nil {
	//			log.Fatalf("error reading signature file: %v", err)
	//		}
	//	}
	//	p.SignatureKey = key
	//}
	//if *baseURL != "" {
	//	var err error
	//	p.DefaultBaseURL, err = url.Parse(*baseURL)
	//	if err != nil {
	//		log.Fatalf("error parsing baseURL: %v", err)
	//	}
	//}
	//p.Timeout = *timeout
	//p.ScaleUp = *scaleUp
	//p.Verbose = *verbose

	server := &http.Server{
		Addr:    *addr,
		Handler: p,
	}
	fmt.Printf("imageproxy listening on %s\n", server.Addr)
	fmt.Printf("Valid image url test is : http://%s/303/https://octodex.github.com/images/codercat.jpg \n", server.Addr)
	fmt.Printf("Invalid image url test is : http://%s/303/https://zzz.zzz.zzz/images/codercat.jpg \n", server.Addr)

	log.Fatal(server.ListenAndServe())
}
