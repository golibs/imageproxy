package imageproxy

import (
	"time"
	"io/ioutil"
	"os"
	"net/http"
	"bytes"
	"strings"
	"path"
)

func GetDir(dir string) string {
	if strings.HasPrefix(dir, ".") {
		currentDir, _ := os.Getwd()
		dir = path.Join(currentDir, dir[1:])
	}
	return dir
}

func GetResponseFromFile(path string, isSetHeader bool) (response *http.Response, err error) {
	var contents []byte
	contents, err = ioutil.ReadFile(path)

	if err == nil {
		fileInfo, _ := os.Stat(path)
		response = ConvertBytesToHttpResponse(contents)
		if isSetHeader {
			response.Header = http.Header{
				"Cache-Control": []string{"max-age=600"},
				"Last-Modified": []string{fileInfo.ModTime().UTC().Format(time.RFC1123)},
				"Date":          []string{time.Now().UTC().Format(time.RFC1123)},
			}
		}
	}
	return response, err
}
func ConvertBytesToHttpResponse(contents []byte) *http.Response {
	response := &http.Response{
		Status:        "200 OK",
		StatusCode:    200,
		Proto:         "HTTP/1.0",
		ProtoMajor:    1,
		ProtoMinor:    0,
		Body:          ioutil.NopCloser(bytes.NewReader(contents)),
		ContentLength: int64(len(contents)),
	}
	return response
}
